//type coercion

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion); //result: 1012
console.log(typeof coercion); //result: string

//none coercion
let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion); //result: 30
console.log(typeof nonCoercion); //result: number

let numE = true + 1;
console.log(numE); //result: 2
console.log(typeof numE); //result: number

//equality operator (==)
console.log(1 == 1); //result: true
console.log(1 == 2); //result: false
console.log(1 == '1'); //result: true
console.log('john' == 'john'); //result: true
console.log('John' == 'john'); //result: false

//inequality operator
console.log(1 != 1); //result: false
console.log(1 != 2); //result: true
console.log(1 != '1'); //result: false
console.log('john' != 'john'); //result: false
console.log('John' != 'john'); //result: true

//strict equality
console.log(1 === 1); //result: true
console.log(1 === '1'); //result: false

//strict inequality
console.log(1 !== 1); //result: false
console.log(1 !== '1'); //result: true

let a = 50;
let b = 65;

//greater than (>)
let isGreaterThan = a > b;
console.log(isGreaterThan); //result: false

//less than (<)
let isLessThan = a < b;
console.log(isLessThan); //result: true

//greater than or Equal (>=)
let isGTE = a >= b;
console.log(isGreaterThan); //result: false

//less than or Equal (<=)
let isLTE = a <= b;
console.log(isLessThan); //result: true

//logic
    //and (&&)
let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

let authorization1 = isAdmin && isRegistered;
console.log(authorization1); //result: false

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2); //result: true

let authorization3 = isAdmin && isLegalAge;
console.log(authorization3); //result: false

    //or (||)
let userLevel = 100;
let userLevel2 = 65;

let userAge = 15;
let userAge2 = 20;

let requiredLevel = 95;
let requiredAge = 18;


let guildRequirement = isRegistered && userLevel >= requiredLevel && userAge >= requiredAge;
console.log(guildRequirement); //result: false

let guildRequirement2 = isRegistered || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
console.log(guildRequirement2); //result: true
